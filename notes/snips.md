const functions = require('firebase-functions');

const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

//==============================================
//  The Routine.
//  1... Device or CURL command writes to ccaccounts
//          snapAcc
//  2... onUpdates write to ccaccounts2
//          addcc
//  3...  AccountUUID2Get account  returns tokens in account.
//==============================================
//==============================================
//  This function fires on an APPEND Event
//  From a Device or a direct CURL command.
//  Used to digest submits from Mobile App.
//  It behaves like a LOG file, but then entire
//  DB can/should be deleted with out affecting system.  
//  ============================================
exports.snapAcc = functions.database
    .ref('/ccaccounts/{pushid}')
    .onWrite(event =>{
        const naccount = event.data.val();
        acc = naccount.account;
        token = naccount.token;
        ts2 = Math.floor(Date.now() / 1000); 

        os = "xx"
        uuid = "uuid"
        action = "action"
        if (naccount.os != null){os = naccount.os}
        if (naccount.uuid != null){uuid = naccount.uuid}
        if (naccount.action != null){action = naccount.action}

        console.log('snapAcc...  added New Account'); 
        console.log('snapAcc...  account = ' + acc);
        console.log('snapAcc...  token = ' +  token); 
        console.log('snapAcc...  os = ' + os); 
        console.log('snapAcc...  UUID = ' + uuid);
        console.log('snapAcc...  action = ' +  action); 


        //  now write this to the db
        // admin.database()
        //         .ref('/newaccount/'+acc)
        //         .set({
        //             "token":token,
        //             "os":os,
        //             "uuid":uuid,
        //             "action":action,
        //             "ts2":ts2
        //         });

            //    var newPostRef = admin.database()
            //     .ref('/newacctok/'+acc+'/tok3/').push();
            //     newPostRef.set({token, ts2, uuid});

            //    var newPostRef = admin.database()
            //     .ref('/newacctok2/'+acc+'/'+uuid+'/').push();
            //     newPostRef.set({token, ts2, uuid});

                var newPostRef = admin.database()
                .ref('/ccaccounts2/'+acc).child(uuid)
                .set({acc, os, token, ts2, uuid, action});
} );
//==============================================
//==============================================


//==============================================
//  Instead of using an APP to post to the DB, 
//  We are using this CURL command instead.
//  This version does not include secret
//==============================================
//https://us-central1-clevercapfb.cloudfunctions.net/addcc?account=testacc&os=android&uuid=testuuid&token=testoken
//https://us-central1-clevercapfb.cloudfunctions.net/addcc?account=testacc2&os=android&uuid=testuuid2&token=testoken
//==============================================
// adds a new record to ccaccounts
// just as would a device
exports.addcc = functions.https.onRequest((req, res) => {
        var account = "account"
        var os = "xx"
        var uuid = "uuid"
        var action = "action"
        var token = "token"
        if (req.query.account != null){account = req.query.account}
        if (req.query.os != null){os = req.query.os}
        if (req.query.uuid != null){uuid = req.query.uuid}
        if (req.query.token != null){token = req.query.token}        
        if (req.query.action != null){action = req.query.action}

        var ts2 = Math.floor(Date.now() / 1000);
        var arr =[];
        arr.push("==================================")
            var json = ({ 
                        "account":account,
                        "os":os,
                        "uuid":uuid,
                        "ts2":ts2,
                        "token":token
                        }); 
            arr.push(json)
            arr.push("==================================")       
            admin.database().ref('/ccaccounts').push(json).then(snapshot => {            
            res.status(200).send(arr);
        });
});
//===================================
//https://us-central1-clevercapfb.cloudfunctions.net/addccsec?account=testacc4&os=android&uuid=testuuid23&token=testokennnnn
//===================================
exports.addccSec = functions.https.onRequest((req, res) => {
        var arr =[];
//  check to confirm secrete works
        var sec = "secrete"
        if (req.query.sec != null){sec = req.query.sec}

    secRef = admin.database().ref('/faccess/'+sec);
    arr.push("============================");
            secRef.once("value")
            .then(function(snapshot) {
                console.log('addccSec ABOIUT TO START');          
                var code_exists = snapshot.child("code").exists();
                    if (code_exists){
                        console.log('addccSec code-exists - '+ code_exists);
                        var code = snapshot.child("code").val(); // {should return code color}
                        arr.push("============================");
                        arr.push("===  secrete = "+ sec);
                        arr.push("====  code = "+ code); 
            //=================================
                    var account = "account"
                    var os = "xx"
                    var uuid = "uuid"
                    var action = "action"
                    var token = "token"
                    if (req.query.account != null){account = req.query.account}
                    if (req.query.os != null){os = req.query.os}
                    if (req.query.uuid != null){uuid = req.query.uuid}
                    if (req.query.token != null){token = req.query.token}        
                    if (req.query.action != null){action = req.query.action}

                    var ts2 = Math.floor(Date.now() / 1000);

                    arr.push("==================================")
                        var json = ({ 
                                    "account":account,
                                    "os":os,
                                    "uuid":uuid,
                                    "ts2":ts2,
                                    "token":token
                                    }); 
                        arr.push(json)
                        arr.push("==================================")       
                        admin.database().ref('/ccaccounts').push(json).then(snapshot => {            
                        res.status(200).send(arr);
                        return;
                    });
                    } else {
                        console.log('addccSec NOT EXIST'); 
                        arr.push("============================");
                        //arr.push("===  account = "+ acc);
                        arr.push("===  secrete = "+ sec);
                        arr.push("====  code no Existe Pendejo!!!!*** ");     
                        res.status(999).send(arr);
                        return;
                    }
            });


});

//===================================


//===================================
//curl 'https://us-central1-clevercapfb.cloudfunctions.net/KeysFromAccountGet01?account=testacc2'
//curl 'https://us-central1-clevercapfb.cloudfunctions.net/AccountUUIDGet?account=testacc2'
//firebase deploy --only functions:KeysFromAccountGet01
exports.AccountUUIDGet = functions.https.onRequest((req, res) => {
  var arr =[];
  var tsnow = Math.floor(Date.now() / 1000);
  //arr.push("===============================");  
  //arr.push("1 - get Tokens for account");
  //arr.push("acount = "+ req.query.account);
  // note change from on() to once()
  admin.database().ref('/ccaccounts2/'+req.query.account).once('value')
    .then(snapshot => {
      snapshot.forEach(miniSnapShot => {
        var tt = miniSnapShot.val();
        var ageSec = tsnow-tt.ts2;
        var ageMin =  Math.floor(ageSec/60);
        var json = ({
          "key":miniSnapShot.key,
          "account":req.query.account,
          "uuid":tt.uuid,
          "ts2":tt.ts2,
          "token":tt.token,
          "ageSeconds":ageSec,
          "ageMinutes":ageMin
        });
        arr.push(json);
      });
      res.status(200).send(arr);
      return;
    });
});

//==============================================
//==============================================
//curl 'https://us-central1-clevercapfb.cloudfunctions.net/KeysFromAccountGet01?account=testacc2'
//curl 'https://us-central1-clevercapfb.cloudfunctions.net/AccountUUIDGet?account=testacc2&secret=xxzzsecrete01'

//Good Secrete key
//https://us-central1-clevercapfb.cloudfunctions.net/AccountUUIDGet2?account=testacc2&sec=xxzzsecrete01

//=====================  failed secret key
//https://us-central1-clevercapfb.cloudfunctions.net/AccountUUIDGet2?account=testacc2&sec=xxxzzsecrete01

//firebase deploy --only functions:KeysFromAccountGet01
exports.AccountUUIDGet2 = functions.https.onRequest((req, res) => {
    sec = req.query.sec;
    secRef = admin.database().ref('/faccess/'+sec);
 var arr =[];
  arr.push("============================");
        secRef.once("value")
        .then(function(snapshot) {
            console.log('code_exists in AccountUUIDGetID2 ABOIUT TO START');          
            var code_exists = snapshot.child("code").exists();
                if (code_exists){
                    console.log('AccountUUIDGetID2 code-exists - '+ code_exists);
                    var code = snapshot.child("code").val(); // {should return code color}
                    arr.push("============================");
                    arr.push("===  secrete = "+ sec);
                    arr.push("====  code = "+ code);     
                } else {
                    console.log('code_exists in AccountUUIDGetID2 NOT EXIST'); 
                    arr.push("============================");
                    //arr.push("===  account = "+ acc);
                    arr.push("===  secrete = "+ sec);
                    arr.push("====  code no Existe Pendejo!!!!*** ");     
                    res.status(999).send(arr);
                }
        });

                    console.log('AccountUUIDGetID2 Starting Part 2');


                var tsnow = Math.floor(Date.now() / 1000);
                admin.database().ref('/ccaccounts2/'+req.query.account).once('value')
                    .then(snapshot => {
                    snapshot.forEach(miniSnapShot => {
                    console.log('AccountUUIDGetID2 About to write step 3');
                        var tt = miniSnapShot.val();
                        var ageSec = tsnow-tt.ts2;
                        var ageMin =  Math.floor(ageSec/60);
                        var json = ({
                        "key":miniSnapShot.key,
                        "account":req.query.account,
                        "uuid":tt.uuid,
                        "ts2":tt.ts2,
                        "token":tt.token,
                        "ageSeconds":ageSec,
                        "ageMinutes":ageMin
                    });
                        console.log('code...  about to arr'); 
                        arr.push(json);
                    });
                    console.log('code...  about to res status'); 
                    res.status(200).send(arr);
                    return;
                    });
                });

//==============================================

//==============================================
//  The Routine.
//  1... Device or CURL command writes to ccaccounts
//          snapAcc
//  2... onUpdates write to ccaccounts2
//          addcc
//  3...  AccountUUID2Get account  returns tokens in account.


//==============================================
//https://us-central1-clevercapfb.cloudfunctions.net/helloWorld
exports.helloWorld = functions.https.onRequest((request, response) => {
 response.send("Hello from Firebase!");
});



//==============================================
//curl 'https://us-central1-clevercapfb.cloudfunctions.net/helloSecret02?secret=badsecrete'
//curl 'https://us-central1-clevercapfb.cloudfunctions.net/helloSecret02?secret=xxzzsecrete01'
exports.helloSecret02 = functions.https.onRequest((req, res)=>{
sec = req.query.secret;
ref = admin.database().ref('/faccess/'+sec);
ref.once("value")
  .then(function(snapshot) {
    var code = snapshot.child("code").val(); // {should return code color}
    var code_exists = snapshot.child("code").exists();
        if (code_exists){
            var arr =[];
            arr.push("============================");
            //arr.push("===  account = "+ acc);
            arr.push("===  secrete = "+ sec);
            arr.push("====  code = "+ code);     
            res.status(200).send(arr);
        } else {
            var arr =[];
            arr.push("============================");
            //arr.push("===  account = "+ acc);
            arr.push("===  secrete = "+ sec);
            arr.push("====  code no Existe Pendejo!!!!*** ");     
            res.status(999).send(arr);
        }
  });
});
//=========================================================================


