Latest version of functions.


1...  This project is basically a polished version of past projects.
I removed test routines that included features such as 1 account 1 device.
Also removed some demonstration functions that are no longer relevant.

2...  The existing CURL command from device is completely intact...
//https://us-central1-clevercapfb.cloudfunctions.net/addcc?account=testacc2&os=android&uuid=testuuid2&token=testoken
This has not changed.  but

3...  I added new addccsec that now requires a secrete key.
//https://us-central1-clevercapfb.cloudfunctions.net/addccSec?account=testacc66&os=android&uuid=testuuid23&token=testokennnnn&sec=xxzzsecrete01
eventually, I will remove addcc function as it is not secure.

4...  Devices can still push additional parameters with not much work, such as LatLong.

5...  addcc and addccsec are both to add accounts from a command line instead of via an app.

6...AccountUUIDGet just needs an account to get all UUID's and Tokens.
curl 'https://us-central1-clevercapfb.cloudfunctions.net/AccountUUIDGet?account=testacc2

7...  same AccountUUIDGet2 but requires a secrete
https://us-central1-clevercapfb.cloudfunctions.net/AccountUUIDGet2?account=testacc2&sec=xxzzsecrete01

All of the queries for Account/UUID no longer have Duplicates.  However, I am not use have to deal with expired TOKENS.  One strategy I am thinking of using is looking at age of Token.  All of these functions will return age in seconds and minutes since last update.


8...  just secrete demonstration
//curl 'https://us-central1-clevercapfb.cloudfunctions.net/helloSecret02?secret=badsecrete'
Also notice that there is a way to assign specirfic parameters to secret keys. The concept is a subscription model.. gold plan, silver plan, Copper plan... The keys can be updated in the DB.  There are no limits to Keys.  I envision this technique if you ever do private label apps for specific customers.
